# Add a star to this repository if you like it :)

# altV-tpMenu

- Simple teleport menu for alt:V by mrv#0002
- Thanks to Weazel#0293 for his [NativeUI](https://github.com/datWeazel/alt-V-NativeUI)

# Preview
- [Video](https://streamable.com/4w12z)

# Configuration

- Change menu open key in client/config.mjs | [List of opening keys](http://keycode.info)
- You can easily add destinations