import * as alt from 'alt';
import * as native from 'natives';
import * as NativeUI from '../includes/src/NativeUI.mjs';
import { Config } from './config.mjs';

const tpMenu = new NativeUI.Menu("Teleport Menu", "Choose your destination", new NativeUI.Point(50, 250));

function Transition(cb) {
    native.doScreenFadeOut(500);

    alt.setTimeout(() => {
        cb();
        native.doScreenFadeIn(1000);
    }, 1500);
}

alt.setTimeout(() => {
    for (let i in Config['locations']) {
        let locationName = Config['locations'][i]['name'];
        let position = Config['locations'][i]['position'];
        let [x, y, z, h] = [position['x'], position['y'], position['z']];

        let posItem = new NativeUI.UIMenuItem(locationName, `Teleport to ~g~${locationName}~s~ ?`);

        tpMenu.AddItem(posItem);

        tpMenu.ItemSelect.on(item => {
            if (item.Text === locationName) {
                Transition(function() {
                    native.setEntityCoords(alt.Player.local.scriptID, x, y, z, false, false, false, true);
                });
            }
        });
    }
}, 100);

alt.on('keydown', (key) => {
    if (key === Config["menuKey"]) {
        tpMenu.Open();
    }
});