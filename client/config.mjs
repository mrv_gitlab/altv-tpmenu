let Config = {
    'menuKey' : 113,

    'locations' : [
        {
            'name' : 'Car Dealer',
            
            'position' : {
                'x' : -74.795,
                'y' : -1109.670,
                'z' : 24.943
            }
        },

        {
            'name' : 'Police Station',

            'position' : {
                'x' : 426.830,
                'y' : -977.525,
                'z' : 29.695
            }
        },

        {
            'name' : 'Maze Bank Area',

            'position' : {
                'x' : -340.786,
                'y' : -1877.578,
                'z' : 28.937
            }
        },

        {
            'name' : 'Vanilla Unicorn',

            'position' : {
                'x' : 138.764,
                'y' : -1310.413,
                'z' : 28.010
            }
        },

        {
            'name' : 'Maze Bank Rooftop',

            'position' : {
                'x' : -75.903,
                'y' : -818.967,
                'z' : 325.173
            }
        }
    ]
};

export { Config }